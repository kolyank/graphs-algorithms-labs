<?php namespace Kolyank\General\Components;

use Cms\Classes\ComponentBase;
use Kolyank\General\Controllers\WeightedGraphController;
use Kolyank\General\Models\Lab4 as Lab4Model;

class Lab4 extends ComponentBase {

    public static $FILE_ORIGINAL = 'lab04.dat';

    public function componentDetails() {
        return [
            'name'        => 'Lab4 Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties() {
        return [];
    }

    public function onRun() {
        $model = new Lab4Model(self::$FILE_ORIGINAL);

        $borovka = $model->spanningTree_Borovka();
        $prim = $model->spanningTree_Prim();
        $kruskal = $model->spanningTree_Kruskal();

        $this->page['graphic'] = [
            'jsons' => [
                'original' => $model->getStructure(),
                'borovka' => WeightedGraphController::getStructureFromData($borovka),
                'prim' => WeightedGraphController::getStructureFromData($prim),
                'kruskal' => WeightedGraphController::getStructureFromData($kruskal)
            ]
        ];

        $this->page['block_1'] = [
            'borovka' => $borovka,
            'prim' => $prim,
            'kruskal' => $kruskal
        ];

    }
}
