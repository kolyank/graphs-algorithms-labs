<?php namespace Kolyank\General\Components;

use Cms\Classes\ComponentBase;
use Kolyank\General\Controllers\BaseController;
use Kolyank\General\Models\Lab2 as Lab2Model;

class Lab2 extends ComponentBase {

    public static $FILE_ORIGINAL = 'lab02.dat';

    public function componentDetails() {
        return [
            'name'        => 'Lab2 Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties() {
        return [];
    }

    public function onRun() {
        $model = new Lab2Model(BaseController::getFileData(self::$FILE_ORIGINAL));
        $costal = new Lab2Model($model->costal(), false);
        $additional = new Lab2Model($model->additional());

        // binding values to page
        $this->page['graphic'] = [
            'jsons' => [
                'original' => $model->getStructure(),
                'costal' => $costal->getStructure(),
                'additional' => $additional->getStructure()
            ]
        ];
        $this->page['block_2'] = [
            'duality' => $model->checkDuality_BFS()
        ];
        $this->page['block_3'] = [
            'min_vertex_cover' => $model->getMinimalVertexCover(),
            'max_independent_vertices' => $model->getMaximumIndependentVertices()
        ];
        $this->page['block_5'] = [
            'max_pairs_combination' => $model->getMaxPairsCombination(),
            'min_edge_cover' => $model->getMinimalEdgeCover()
        ];
        $this->page['block_6'] = [
            'dominant_vertices' => $model->getDominantVertices(),
            'dominant_edges' => $model->getDominantEdges()
        ];
    }
}
