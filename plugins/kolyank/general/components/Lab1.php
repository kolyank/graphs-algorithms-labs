<?php namespace Kolyank\General\Components;

use Cms\Classes\ComponentBase;
use Kolyank\General\Controllers\BaseController;
use Kolyank\General\Models\Lab1 as Lab1Model;

class Lab1 extends ComponentBase {

    public static $FILE_ORIGINAL = 'lab01.dat';

    public function componentDetails() {
        return [
            'name'        => 'Lab1 Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties() {
        return [];
    }

    public function onRun() {
        $model = new Lab1Model(BaseController::getFileData(self::$FILE_ORIGINAL));
        $simplified = new Lab1Model($model->simplify());

        $verticesDegrees = $model->getVerticesDegrees();    // 0 - isolated, 1 - hanging
        $simplifiedDFS = $simplified->calculateDFSes();
        $simplifiedBFS = $simplified->calculateBFSes();

        // binding values to page
        $this->page['graphic'] = [
            'jsons' => [
                'original' => $model->getStructure(),
                'simplified' => $simplified->getStructure()
            ]
        ];
        $this->page['block_1'] = [
            'matrix' => $model->getAdjacencyMatrixForInterface(),
            'list' => $model->adjacency_list
        ];
        $this->page['block_2'] = [
            'vertices' => [
                'degrees' => $verticesDegrees,
                'isolated' => array_filter($verticesDegrees, function ($item) { return $item === 0; }),
                'hanging' => array_filter($verticesDegrees, function ($item) { return $item === 1; }),
                'looped' => $model->getVerticesWithLoops()
            ],
            'edges' => [
                'hanging' => $model->getHangingEdges(),
                'multiple' => $model->getMultipleEdges()
            ]
        ];
        $this->page['block_3'] = [
            'list' => $simplified->adjacency_list
        ];
        $this->page['block_4'] = [
            'components' => $simplified->components
        ];
        $this->page['block_5'] = [
            'dfs' => $simplifiedDFS,
            'bfs' => $simplifiedBFS
        ];
        $this->page['block_5_graphics'] = [
            'dfs' => $simplified->calculateDFSGraphs($simplifiedDFS),
            'bfs' => $simplified->calculateBFSGraphs($simplifiedBFS)
        ];
    }
}
