<?php namespace Kolyank\General\Components;

use Cms\Classes\ComponentBase;
use Kolyank\General\Models\Lab3 as Lab3Model;
use Kolyank\General\Controllers\BaseController;

class Lab3 extends ComponentBase {

    public static $FILE_ORIGINAL = 'lab02.dat';

    public function componentDetails() {
        return [
            'name'        => 'Lab3 Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties() {
        return [];
    }

    public function onRun() {
        $model = new Lab3Model(BaseController::getFileData(self::$FILE_ORIGINAL));

        // binding values to page
        $this->page['graphic'] = [
            'jsons' => [
                'vertices_colorized' => $model->getColorizedVerticesStructure(),
                'edges_colorized' => $model->getColorizedEdgesStructure(),
                'spanning_tree_bfs' => BaseController::getStructureFromData($model->spanningTree_BFS($model->adjacency_list))
            ]
        ];
        $this->page['block_1'] = [
            'vertices_chromatic' => $model->getChromaticNumber(),
            'edges_chromatic' => $model->getChromaticIndex()
        ];
        $this->page['block_2'] = [
            'opened_walk' => $model->getOpenedWalk(),
            'closed_walk' => $model->getClosedWalk(),
            'trail' => $model->getTrail(),
            'simple_trail' => $model->getSimpleTrail(),
            'circuit' => $model->getCircuit(),
            'simple_circuit' => $model->getSimpleCircuit()
        ];
        $this->page['block_3'] = [
            'euler_path' => $model->getEulerPath(),
            'euler_circuit' => $model->getEulerCircuit(),
            'hamilton_path' => $model->getHamiltonPath(),
            'hamilton_circuit' => $model->getHamiltonCircuit()
        ];
        $this->page['block_4'] = [
            'connectivity_count' => $model->getConnectivityComponentsCount(),
            'girth' => $model->getMaxComponentGirth(),
            'circumference' => $model->getMaxComponentCircumference(),
            'diameter' => $model->getMaxComponentDiameter()
        ];
        $this->page['block_5'] = [
            //
        ];
    }
}
