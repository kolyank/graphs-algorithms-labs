<?php namespace Kolyank\General;

use Backend;
use Kolyank\General\Components\Lab1;
use Kolyank\General\Components\Lab2;
use Kolyank\General\Components\Lab3;
use Kolyank\General\Components\Lab4;
use System\Classes\PluginBase;

/**
 * General Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'General',
            'description' => 'No description provided yet...',
            'author'      => 'Kolyank',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            Lab1::class => 'lab1',
            Lab2::class => 'lab2',
            Lab3::class => 'lab3',
            Lab4::class => 'lab4'
        ];

        return [
            'Kolyank\General\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'kolyank.general.some_permission' => [
                'tab' => 'General',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'general' => [
                'label'       => 'General',
                'url'         => Backend::url('kolyank/general/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['kolyank.general.*'],
                'order'       => 500,
            ],
        ];
    }
}
