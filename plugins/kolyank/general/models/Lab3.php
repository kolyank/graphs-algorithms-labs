<?php namespace Kolyank\General\Models;

use Kolyank\General\Controllers\BaseController;

class Lab3 extends Lab2 {

//    1. Выполнить вершинную и рёберную раскраски; дать оценку хроматического числа и хроматического индекса графа.
//    2. Привести примеры: закрытого и открытого маршрутов, цепи, простой цепи (пути), цикла (контура) и простого цикла.
//    3. Привести примеры: эйлерова пути и эйлерова цикла, гамильтонова пути и гамильтонова цикла (или показать, что их в данном графе нет).
//    4. Определить количество компонент связности графа и для максимальной связной компоненты определить обхват, окружение и диаметр.
//    5. Проиллюстрировать работу алгоритмов поиска в глубину и в ширину.
//    6. Построить остовное дерево (или остовный лес) графа.

    public $v0;

    public function __construct($data, $calculate_parameters = true) {
        parent::__construct($data, $calculate_parameters);
    }

    public function getColorizedVertices() {
        $vertices = $this->getVerticesDegrees();
        arsort($vertices);
        foreach ($vertices as $key=>$vertex) {
            $vertices[$key] = $this->adjacency_list[$key];
        }

        $color = 1;

        $current_vertex = key($vertices);
        $colored = $vertices[$current_vertex];

        $res = [
            $current_vertex => $color
        ];

        // TODO: error at the end of last iteration means algorithm ends
        try {
            while (count($res) !== count($this->vertices)) {
                foreach ($vertices as $key=>$value) {
                    if (!in_array((string) $key, $colored)) {
                        $res[$key] = $color;
                        $colored = array_merge($colored, $vertices[$key]);
                    }
                }

                $color++;

                $vertices = array_filter($vertices, function ($key) use ($res) { return !array_key_exists($key, $res); }, ARRAY_FILTER_USE_KEY);

                $current_vertex = key($vertices);

                $colored = $vertices[$current_vertex];
                $res[$current_vertex] = $color;
                next($vertices);
            }
        } catch (\Exception $e) {
            return $res;
        }

        return $res;
    }

    public function getColorizedVerticesStructure() {
        $colorizedVertices = $this->getColorizedVertices();

        $edges = [];
        $nodes = [];
        foreach ($colorizedVertices as $key=>$vertex) {
            $nodes[] = [
                'id' => (string) $key,
                'label' => (string) $key,
                'color' => BaseController::$COLORS[$vertex]
            ];
        }

        foreach ($this->data as $key=>$edge) {
            $edges[] = [
                'from' => $edge[0],
                'to' => $edge[1],
                'color' => [
                    'color' => BaseController::$DEFAULT_COLOR
                ]
            ];
        }

        return [
            'nodes' => $nodes,
            'edges' => $edges
        ];
    }

    public function getChromaticNumber() {
        return count(array_unique(array_values($this->getColorizedVertices())));
    }

    public function getColorizedEdges() {
        $edges = $this->data;

        $color = 1;
        $current_edge_key = key($edges);
        $current_edge = $edges[$current_edge_key];
        $colored = $edges[$current_edge_key];

        $res = [
            $color => [ $current_edge ]
        ];

        // TODO: error at the end of last iteration means algorithm ends
        try {
            while (count(array_reduce($res, 'array_merge', [])) !== ($this->data)) {
                foreach ($edges as $edge) {
                    if (!in_array($edge[0], $colored) && !in_array($edge[1], $colored)) {
                        $res[$color][] = $edge;
                        $colored = array_merge($colored, $edge);
                    }
                }

                $color++;

                foreach ($res as $color_block) {
                    $edges = BaseController::array_diff_multidimensional($edges, $color_block);
                }

                $current_edge_key = key($edges);
                $current_edge = $edges[$current_edge_key];
                $colored = $edges[$current_edge_key];
                $res[$color][] = $current_edge;
            }
        } catch (\Exception $e) {
            return $res;
        }

        return $res;
    }

    public function getColorizedEdgesStructure() {
        $data = $this->getColorizedEdges();

        $used_vertices = [];

        $edges = [];
        $nodes = [];
        foreach ($data as $key=>$colored_data) {
            foreach ($colored_data as $edge) {
                $edges[] = [
                    'from' => $edge[0],
                    'to' => $edge[1],
                    'color' => [
                        'color' => BaseController::$COLORS[$key]
                    ]
                ];

                foreach ($edge as $vertex) {
                    if (!in_array($vertex, $used_vertices)) {
                        $nodes[] = [
                            'id' => $vertex,
                            'label' => $vertex,
                            'color' => [
                                'color' => BaseController::$DEFAULT_COLOR
                            ]
                        ];
                        $used_vertices[] = $vertex;
                    }
                }
            }
        }

        return [
            'nodes' => $nodes,
            'edges' => $edges
        ];
    }

    public function getChromaticIndex() {
        return count($this->getColorizedEdges());
    }

    public function getOpenedWalk() {
        $count = rand(2, count($this->vertices) * 2);

        $key = (string) array_rand($this->adjacency_list);
        $list = $this->adjacency_list[$key];

        $res = [];

        $i = 0;
        while ($i < $count) {
            $res[] = (string) $key;

            $tmp = array_rand($list);
            $key = (string) $list[$tmp];
            $list = $this->adjacency_list[$key];

            $i++;
        }

        return $res;
    }

    public function getClosedWalk() {
        $key = (string) array_rand($this->adjacency_list);
        $list = $this->adjacency_list[$key];

        $res = [];

        $stop = false;
        while (!$stop) {
            $res[] = (string) $key;

            if (count($res) > 1 && $key === $res[0]) {
                break;
            }

            $tmp = array_rand($list);
            $key = (string) $list[$tmp];
            $list = $this->adjacency_list[$key];
        }

        return $res;
    }

    public function getTrail() {
        do {
            $stop = true;
            $walk = $this->getOpenedWalk();

            $edges = [];
            for ($i = 0; $i < count($walk) - 1; $i++) {
                $edge = [ $walk[$i], $walk[$i + 1] ];
                if (!in_array($edge, $edges)) {
                    $edges[] = $edge;
                } else {
                    $stop = false;
                }
            }
        } while (!$stop);

        return $walk;
    }

    public function getSimpleTrail() {
        do {
            $walk = $this->getOpenedWalk();
        } while (count($walk) != count(array_unique($walk)));

        return $walk;
    }

    public function getCircuit() {
        do {
            $stop = true;
            $walk = $this->getClosedWalk();

            $edges = [];
            for ($i = 0; $i < count($walk) - 1; $i++) {
                $edge = [ $walk[$i], $walk[$i + 1] ];
                if (!in_array($edge, $edges)) {
                    $edges[] = $edge;
                } else {
                    $stop = false;
                }
            }
        } while (!$stop);

        return $walk;
    }

    public function getSimpleCircuit() {
        do {
            $circuit = $this->getCircuit();
            // first and last vertices are same because this is a variant of closed walk
        } while (count(array_unique($circuit)) < 3 || count($circuit) - 1 != count(array_unique($circuit)));

        return $circuit;
    }

    // The Euler path in a graph exists if and only if the graph is connected and contains at most two vertices of odd degree (0 - is cycle or 2).
    public function getEulerPath() {
        $oddVerticesCount = count(array_filter($this->adjacency_list, function ($item) { return count($item) % 2 !== 0; }));

        if ($this->getConnectivityComponentsCount() > 1 || !in_array($oddVerticesCount, [ 0, 2 ])) {
            return false;
        }

        $res = [];

        return $res;
    }

    // The Euler path in a graph exists if and only if the graph is connected and contains no vertices of odd degree.
    public function getEulerCircuit() {
        $oddVerticesCount = count(array_filter($this->adjacency_list, function ($item) { return count($item) % 2 !== 0; }));

        if ($this->getConnectivityComponentsCount() > 1 || $oddVerticesCount > 0) {
            return false;
        }

        $res = [];

        return $res;
    }

    public function getHamiltonPath() {
        foreach ($this->vertices as $vertex) {
            $this->v0 = (int) $vertex;

            $this->used_temp = array_fill(0, count($this->vertices), -1);
            $this->temp = array_fill(0, count($this->vertices), 0);

            $this->temp[0] = $this->v0;
            $this->used_temp[$this->v0] = $this->v0;

            $hamilton = $this->getHamiltonPath_Recursion($this->v0);

            if ($hamilton) {
                $res = [];
                for ($j = 1; $j < count($this->vertices); $j++) {
                    $res[] = $this->temp[$j] + 1;
                }
                $res[] = $this->temp[0] + 1;

                return $res;
            }
        }

        return false;
    }

    public function getHamiltonPath_Recursion($k) {
        $found = false;
        $n = count($this->vertices);

        for ($v = 0; $v < $n && !$found; $v++) {
            if ($this->adjacency_matrix[$v][$this->temp[$k - 1]] || $this->adjacency_matrix[$this->temp[$k - 1]][$v]) {
                if ($k === $n && $v === $this->v0) {
                    $found = true;
                } elseif ($this->used_temp[$v] == -1) {
                    $this->used_temp[$v] = $k;
                    $this->temp[$k] = $v;
                    $found = $this->getHamiltonPath_Recursion($k + 1);
                    if (!$found) {
                        $this->used_temp[$v] = -1;
                    }
                } else {
                    continue;
                }
            }
        }

        return $found;
    }

    // Not exists if graph has vertices with degree less then 2
    public function getHamiltonCircuit() {
        if (count(array_filter($this->getVerticesDegrees(), function ($item) { return $item < 2; })) !== 0) {
            return false;
        }

        $res = [];

        return $res;
    }

    public function getConnectivityComponentsCount() {
        return count($this->components);
    }

    public function getMaxComponent() {
        $components = $this->getConnectivityComponentsGraphs();

        uksort($components, function($item1, $item2) { return count($item2) - count($item1); });

        return $components[0];
    }

    public function getMaxComponentGirth() {
        $component = $this->getMaxComponent();

        $data = BaseController::getDataFromAdjacencyList($component);

        $graph = new self($data);

        $res = $graph->vertices;

        $iteration = 1000;
        while ($iteration > 0) {
            $simpleCircuit = $graph->getSimpleCircuit();
            if (count($simpleCircuit) < count($res)) {
                $res = $simpleCircuit;
            }

            $iteration--;
        }

        // first and last are the same vertex
        return count($res) - 1;
    }

    public function getMaxComponentCircumference() {
        $component = $this->getMaxComponent();

        $data = BaseController::getDataFromAdjacencyList($component);

        $graph = new self($data);

        $res = [];

        $iteration = 1000;
        while ($iteration > 0) {
            $simpleCircuit = $graph->getSimpleCircuit();
            if (count($simpleCircuit) > count($res)) {
                $res = $simpleCircuit;
            }

            $iteration--;
        }

        // first and last are the same vertex
        return count($res) - 1;
    }

    public function getMaxComponentDiameter() {
        $component = $this->getMaxComponent();

        $data = BaseController::getDataFromAdjacencyList($component);

        $graph = new self($data);

        $min = min($graph->vertices);
        $max = max($graph->vertices);
        $v = $min;
        $u = $min;
        $w = $min;

        $d = $this->getMaxComponentDiameter_BFS($graph->adjacency_list, $v);
        for ($i = $min; $i <= $max; $i++) {
            if ($d[$i] > $d[$u]) {
                $u = $i;
            }
        }

        $d = $this->getMaxComponentDiameter_BFS($graph->adjacency_list, $u);
        for ($i = $min; $i <= $max; $i++) {
            if ($d[$i] > $d[$w]) {
                $w = $i;
            }
        }

        return $d[$w];
    }

    public function getMaxComponentDiameter_BFS($graph, $start = '1') {
        $visited = [];
        $output = [
            (int) $start => 0
        ];
        $queue = [];

        $queue[] = $graph[$start];
        $visited[$start] = true;

        while(count($queue) > 0) {
            $index = 1;
            $currentNodeAdj = array_pop($queue);

            foreach ($currentNodeAdj as $vertex) {
                if (!array_key_exists($vertex, $visited)) {
                    array_unshift($queue, $graph[$vertex]);
                    $output[$vertex] = $index;
                    $index++;
                }

                $visited[$vertex] = true;
            }
        }

        return $output;
    }
}
