<?php namespace Kolyank\General\Models;

class Lab2 extends Lab1 {

//    1. Построить исходный граф и его рёберный граф; показать соответствие между вершинами исходного графа и кликами рёберного графа
//    2. Выполнить проверку графа на двудольность путем его обхода методом поиска в ширину (BFS)
//    3. Определить наименьшее вершинное покрытие и максимальное независимое множество вершин с помощью эвристического «жадного» алгоритма
//    4. Построить граф, дополнительный к исходному, и показать соответствие между максимальным независимым множеством вершин исходного графа и максимальной кликой дополнительного графа
//    5. Найти максимальное паросочетание и наименьшее рёберное покрытие с помощью эвристического «жадного» алгоритма
//    6. Построить наименьшее доминирующее множество вершин и наименьшее доминирующее множество рёбер исходного графа

    public function __construct($data, $calculate_parameters = true) {
        parent::__construct($data, $calculate_parameters);
    }

    public function getVertices() {
        return $this->getVerticesFromData();
    }

    public function checkDuality_BFS() {
        $min = min($this->vertices);
        $max = max($this->vertices);
        $queue = [];
        $part = array_fill($min, $max, -1);

        $res = false;

        for ($i = $min; $i < $max; ++$i) {
            if ($part[$i] === -1) {
                $h = $min;
                $t = $min;
                $queue[$t++] = $i;
                $part[$i] = 0;
                while ($h < $t) {
                    $vertex = $queue[$h++];
                    for ($j = 0; $j < count($this->adjacency_list[$vertex]); ++$j) {
                        $to = $this->adjacency_list[$vertex][$j];
                        if ($part[$to] === -1) {
                            $part[$to] = !$part[$vertex];
                            $queue[$t++] = $to;
                        } elseif ($part[$to] === $part[$vertex] && !$res) {
                            $res = false;
                        }
                    }
                }
            }
        }

        return $res;
    }

    public function getMinimalVertexCover() {
        $degrees = $this->getVerticesDegrees();
        $edges = $this->data;
        $res = [];

        while (count($edges)) {
            $max_degree_vertices = array_keys($degrees, max($degrees));
            $max_degree_vertex = (string) $max_degree_vertices[0];
            $res[] = $max_degree_vertex;

            foreach ($edges as $edge_key=>$edge) {
                if (in_array($max_degree_vertex, $edge)) {
                    foreach ($degrees as $degree_key=>$degree) {
                        if (in_array($degree_key, $edge) && $degree_key != $max_degree_vertex) {
                            $degrees[$degree_key]--;
                        }
                    }
                    unset($edges[$edge_key]);
                }
            }

            unset($degrees[$max_degree_vertex]);
        }

        return $res;
    }

    public function getMaximumIndependentVertices() {
        $degrees = $this->getVerticesDegrees();
        $edges = $this->data;
        $res = [];

        while (count($degrees)) {
            $max_degree_vertices = array_keys($degrees, min($degrees));
            $max_degree_vertex = (string) $max_degree_vertices[0];
            $res[] = $max_degree_vertex;

            foreach ($edges as $edge_key=>$edge) {
                if (in_array($max_degree_vertex, $edge)) {
                    foreach ($degrees as $degree_key=>$degree) {
                        if (in_array($degree_key, $edge)) {
                            unset($degrees[$degree_key]);
                        }
                    }
                    unset($edges[$edge_key]);
                }
            }
        }

        return $res;
    }

    public function getMinimalEdgeCover() {
        $res = $this->getMaxPairsCombination();
        $res_reduced = array_reduce($res, 'array_merge', []);

        if (count($res_reduced) === count($this->vertices)) {
            return $res;
        } else {
            $unused = array_diff($res_reduced, $this->vertices);

            while (count($unused)) {
                $vertex = $unused[0];
                $vertex_key = 0;

                foreach ($this->data as $edge) {
                    if (in_array($vertex, $edge)) {
                        $res[] = $edge;
                        unset($unused[$vertex_key]);
                    }
                }
            }

            return $res;
        }

    }

    public function getMaxPairsCombination() {
        $degrees = $this->getVerticesDegrees();
        $edges = $this->data;
        $isolated = [];
        $res = [];

        $iterator = 0;
        while (count($this->vertices) != count($isolated)) {
            $min_degree_vertices = array_keys($degrees, min($degrees));
            $min_degree_vertex = (string) $min_degree_vertices[0];

            foreach ($edges as $edge_key=>$edge) {
                if (in_array($min_degree_vertex, $edge) && !in_array($min_degree_vertex, $isolated)) {
                    $res[] = $edge;

                    foreach ($edge as $vertex) {
                        $isolated[] = (int) $vertex;
                        $degrees[$vertex]--;

                        foreach ($edges as $edge_key_2=>$edge2) {
                            if (in_array($vertex, $edge2)) {
                                unset($edges[$edge_key_2]);
                            }
                        }
                    }
                }
            }

            $degrees = array_filter($degrees, function ($item, $key) use ($isolated) { return $item > 0 && !in_array($key, $isolated); }, ARRAY_FILTER_USE_BOTH);

            $iterator++;
        }

        return $res;
    }

    public function getDominantVertices() {
        $degrees = $this->getVerticesDegrees();
        $res = [];

        while (count($degrees)) {
            $max_degree_vertices = array_keys($degrees, min($degrees));
            $max_degree_vertex = (string) $max_degree_vertices[0];
            $res[] = $max_degree_vertex;

            unset($degrees[$max_degree_vertex]);
            foreach ($this->adjacency_list[$max_degree_vertex] as $vertex) {
                unset($degrees[$vertex]);
            }
        }

        return $res;
    }

    public function getDominantEdges() {
        // max pairs combination is dominant edges combination
        return $this->getMaxPairsCombination();
    }

}
