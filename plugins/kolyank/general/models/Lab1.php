<?php namespace Kolyank\General\Models;

use Kolyank\General\Controllers\BaseController;
use Exception;

class Lab1 {

    //    1. Строит и выводит на экран списки смежности и матрицу смежности
    //    2. Определяет:
    //    ◦ степени всех вершин и выводит список вершин с их степенями;
    //    ◦ изолированные вершины и выводит их список;
    //    ◦ висячие вершины и выводит их список;
    //    ◦ висячие ребра и выводит их список;
    //    ◦ вершины, в которых имеются петли, и выводит список таких вершин с кратностями петель;
    //    ◦ кратные ребра и выводит их список с кратностями
    //    3. Приводит исходный граф к форме простого графа: удаляет петли и для кратных ребер оставляет только одно ребро. Для преобразованного графа выводит списки смежности
    //    4. Определяет количество компонент связности преобразованного графа и выводит на экран состав каждой компоненты в форме списка входящих в компоненту вершин
    //    5. Для каждой не тривиальной компоненты связности строит остовное дерево двумя способами: с помощью алгоритма поиска в глубину (DFS) и с помощью алгоритма поиска в ширину (BFS)

    public $data;
    public $vertices;
    public $adjacency_list;
    public $adjacency_matrix;
    public $components;
    public $temp = [];
    public $used_temp;

    public function __construct($data, $calculate_parameters = true) {
        $this->data = $data;
        $this->vertices = $this->getVertices();

        if ($calculate_parameters) {
            $this->initUsed();

            $this->adjacency_list = $this->getAdjacencyList();
            $this->adjacency_matrix = $this->getAdjacencyMatrix();
            $this->components = $this->getConnectivityComponents();
        }
    }

    public function initUsed() {
        $this->used_temp = [];

        foreach ($this->vertices as $vertex) {
            $this->used_temp[$vertex] = false;
        }
    }

    public function clearTemp() {
        $this->temp = [];
    }

    public function getVertices() {
        $temp = [];
        foreach ($this->data as $edge) {
            $temp = array_merge($temp, $edge);
        }

        $min = 1;
        $max = (int) max($temp);

        $vertices = [];
        for ($i = $min; $i <= $max; $i++) {
            $vertices[] = (string) $i;
        }

        return $vertices;
    }

    public function getVerticesFromData() {
        $vertices = array_unique(explode(BaseController::$SPACE_DELIMITER, implode(BaseController::$SPACE_DELIMITER, array_map(function ($item) {
            return implode(BaseController::$SPACE_DELIMITER, $item);
        }, $this->data))));

        sort($vertices);

        return $vertices;
    }

    public function getStructure() {
        $nodes = [];
        $edges = [];

        foreach ($this->vertices as $vertex) {
            $nodes[] = [
                'id' => $vertex,
                'label' => $vertex
            ];
        }

        foreach ($this->data as $key=>$edge) {
            $edges[] = [
                'from' => $edge[0],
                'to' => $edge[1],
                'color' => [
                    'color' => BaseController::$DEFAULT_COLOR
                ]
            ];
        }

        return [
            'nodes' => $nodes,
            'edges' => $edges
        ];
    }

    public function getAdjacencyMatrix() {
        $vertices = $this->vertices;
        $size = max($vertices);
        $matrix = [];
        for ($i = 0; $i < $size; $i++) {
            $matrix[$i] = array_fill(0, $size, 0);
        }

        foreach ($this->data as $edge) {
            $matrix[$edge[0]-1][$edge[1]-1] += 1;
            $matrix[$edge[1]-1][$edge[0]-1] += 1;
        }

        for ($i = 0; $i < $size; $i++) {
            if (!in_array($i+1, $vertices)) {
                foreach ($matrix as &$row) {
                    unset($row[$i]);
                }
                unset($matrix[$i]);
            }
        }

        return $matrix;
    }

    public function getAdjacencyMatrixForInterface() {
        $matrix = $this->getAdjacencyMatrix();
        $vertices = $this->vertices;

        array_unshift($matrix, $vertices);
        try {
            foreach ($matrix as $key=>&$row) {
                array_unshift($matrix[$key + 1], $vertices[$key]);
            }
        } catch (Exception $e) {
            //
        }

        array_unshift($matrix[0], '');

        return array_filter($matrix, function ($item) { return $item; });
    }

    public function getAdjacencyList($vertices = []) {
        $vertices = $vertices ?: $this->vertices;

        $list = [];
        foreach ($vertices as $vertex) {
            $list_item = [];
            foreach ($this->data as $edge) {
                if (in_array($vertex, $edge)) {
                    $tmp = $edge;
                    unset($tmp[array_search($vertex, $edge)]);
                    $list_item[] = array_shift($tmp);
                }
            }
            $list[$vertex] = $list_item;
        }

        return $list;
    }

    // 0 - isolated
    // 1 - hanging
    public function getVerticesDegrees() {
        $res = [];
        foreach ($this->vertices as $vertex) {
            $count = 0;
            foreach ($this->data as $edge) {
                if (in_array($vertex, $edge)) {
                    $count++;
                }
            }
            $res[$vertex] = $count;
        }

        return $res;
    }

    public function getVerticesWithLoops() {
        $res = [];
        foreach ($this->vertices as $vertex) {
            $count = 0;
            foreach ($this->data as $edge) {
                if ($edge[0] === $vertex && $edge[1] === $vertex) {
                    $count++;
                }
            }
            $res[$vertex] = $count;
        }

        return array_filter($res, function ($item) {
            return $item > 0;
        });
    }

    public function getHangingEdges() {
        $res = [];

        $hangingVertices = array_keys(array_filter($this->getVerticesDegrees(), function ($item) { return $item === 1; }));
        foreach ($this->data as $edge) {
            if (count(array_intersect($hangingVertices, $edge))) {
                $res[] = $edge;
            }
        }

        return $res;
    }

    public function getMultipleEdges() {
        $res = [];
        foreach ($this->data as $key1=>$edge1) {
            $found = false;
            foreach ($this->data as $key2=>$edge2) {
                if ($key1 !== $key2 && !count(array_diff($edge1, $edge2))) {
                    $found = true;
                }
            }

            if ($found) {
                $res[] = $edge1;
            }
        }

        return $res;
    }

    public function getConnectivityComponents() {
        $min = min($this->vertices);
        $max = max($this->vertices);

        $res = [];

        for ($i = $min; $i <= $max; ++$i) {
            if (!$this->used_temp[$i]) {
                $this->clearTemp();
                $this->getConnectivityComponents_DFS($i);

                $res[] = $this->temp;
            }
        }

        return $res;
    }

    public function getConnectivityComponents_DFS($v) {
        $this->used_temp[$v] = true;
        $this->temp[] = $v;

        for ($i = 0; $i < count($this->adjacency_list[$v]); ++$i) {
            $to = $this->adjacency_list[$v][$i];

            if (!$this->used_temp[$to]) {
                $this->getConnectivityComponents_DFS($to);
            }
        }
    }

    public function getConnectivityComponentsGraphs() {
        $components = array_filter($this->components, function ($item) { return count($item) > 1; });

        $res = [];
        foreach ($components as $key=>$component) {
            foreach ($component as $vertex) {
                $res[$key][$vertex] = $this->adjacency_list[$vertex];
            }
        }

        return $res;
    }

    public function calculateDFSGraphs($dfses) {
        $res = [];
        foreach ($dfses as $dfs) {
            $res[] = BaseController::getStructureFromData($dfs);
        }

        return $res;
    }

    public function calculateBFSGraphs($bfses) {
        $res = [];
        foreach ($bfses as $bfs) {
            $res[] = BaseController::getStructureFromData($bfs);
        }

        return $res;
    }

    public function calculateDFSes() {
        $graphs = $this->getConnectivityComponentsGraphs();

        $res = [];
        foreach ($graphs as $graph) {
            $res[] = $this->spanningTree_DFS($graph, current(array_keys($graph)), true);
        }

        return $res;
    }

    public function calculateBFSes() {
        $graphs = $this->getConnectivityComponentsGraphs();

        $res = [];
        foreach ($graphs as $graph) {
            $res[] = $this->spanningTree_BFS($graph, current(array_keys($graph)));
        }

        return $res;
    }

    public function spanningTree_DFS($graph, $start = '1', $reset = false) {
        global $visited;
        global $output;

        if ($reset) {
            $output = [];
        }

        $visited[] = $start;

        foreach ($graph[$start] as $index => $vertex) {
            if (!in_array($vertex, $visited)) {
                $output[] = [ (string) $start, $vertex ];
                $this->spanningTree_DFS($graph, $vertex);
            }
        }

        return $output;
    }

    public function spanningTree_BFS($graph, $start = '1') {
        $visited = [];
        $output = [];
        $queue = [];

        $queue[] = $graph[$start];
        $visited[$start] = true;

        while(count($queue) > 0) {
            $currentNodeAdj = array_pop($queue);

            foreach ($currentNodeAdj as $vertex) {
                if (!array_key_exists($vertex, $visited)) {
                    array_unshift($queue, $graph[$vertex]);
                    $output[$start] = [ (string) array_keys($graph, $currentNodeAdj)[0], $vertex];
                    $start = $vertex;
                }

                $visited[$vertex] = true;
            }
        }

        return $output;
    }

    public function simplify() {
        $simplified_data = $this->data;

        foreach ($simplified_data as $key1=>$edge1) {
            if ($edge1[0] === $edge1[1]) {
                unset($simplified_data[$key1]);
                continue;
            }

            foreach ($simplified_data as $key2=>$edge2) {
                if ($key1 !== $key2 && !count(array_diff($edge1, $edge2))) {
                    unset($simplified_data[$key1]);
                }
            }
        }

        return $simplified_data;
    }

    public function costal() {
        $data = $this->simplify();

        $vertices = [];
        foreach ($data as $edge) {
            $vertices[] = sprintf('%s-%s', $edge[0], $edge[1]);
        }

        $costal_data = [];
        foreach ($vertices as $vertex1) {
            $spitted = explode(BaseController::$HYPHEN_DELIMITER, $vertex1);

            foreach ($spitted as $value) {
                foreach ($vertices as $vertex2) {
                    $spitted2 = explode(BaseController::$HYPHEN_DELIMITER, $vertex2);

                    if ($vertex1 !== $vertex2 && in_array($value, $spitted2)) {
                        $add = true;
                        foreach ($costal_data as $costal_datum) {
                            if (($costal_datum[0] === $vertex1 && $costal_datum[1] === $vertex2) || ($costal_datum[0] === $vertex2 && $costal_datum[1] === $vertex1)) {
                                $add = false;
                            }
                        }

                        if ($add) {
                            $costal_data[] = [$vertex1, $vertex2];
                        }
                    }
                }
            }
        }

        return $costal_data;
    }

    public function additional() {
        $vertices = $this->vertices;
        $adjacency_list = $this->adjacency_list;

        $additional_data = [];
        foreach ($adjacency_list as $key=>$adjacencies) {
            $key = (string) $key;
            $diff = array_diff($vertices, $adjacencies);

            foreach ($diff as $item) {
                if ($item !== $key) {
                    $add = true;
                    foreach ($additional_data as $additional_datum) {
                        if (($additional_datum[0] === $item && $additional_datum[1] === $key) || ($additional_datum[0] === $key && $additional_datum[1] === $item)) {
                            $add = false;
                        }
                    }

                    if ($add) {
                        $additional_data[] = [ $key, $item ];
                    }
                }
            }
        }

        return $additional_data;
    }

}
