<?php namespace Kolyank\General\Models;

use Kolyank\General\Controllers\WeightedGraphController;

class Lab4 {

    //    Формат входного файла: первая строка – количество вершин графа, вторая и последующие строки – ребра графа в формате «вершина-вершина-вес».
    //
    //    1. Разработать и реализовать 3 программы потроения минимального остовного дерева по алгоритмам: Борувки, Ярника (Прима) и Крускала.


    //
    // https://ru.wikipedia.org/wiki/Алгоритм_Борувки || https://neerc.ifmo.ru/wiki/index.php?title=Алгоритм_Борувки
    // http://e-maxx.ru/algo/mst_prim
    // http://e-maxx.ru/algo/mst_kruskal

    // [ [vertex1 - vertex2 - weight], ... ]
    public $data;
    public $vertices;
    public $adjacency_list;
    public $temp = [];
    public $used_temp = [];

    public function __construct($file) {
        $this->vertices = range(1, WeightedGraphController::getVertexCountFromFile($file));
        $this->data = WeightedGraphController::getFileData($file);
        $this->adjacency_list = $this->getAdjacencyList();

        $this->initUsed();
    }

    public function initUsed() {
        $this->used_temp = [];

        foreach ($this->vertices as $vertex) {
            $this->used_temp[$vertex] = false;
        }
    }

    public function clearTemp() {
        $this->temp = [];
    }

    public function getAdjacencyList($vertices = []) {
        $vertices = $vertices ?: $this->vertices;

        $list = [];
        foreach ($vertices as $vertex) {
            $list_item = [];
            foreach ($this->data as $edge) {
                $edge_without_weight = WeightedGraphController::getEdgeWithoutWeight($edge);
                if (in_array($vertex, $edge_without_weight)) {
                    $tmp = $edge_without_weight;
                    unset($tmp[array_search($vertex, $edge_without_weight)]);
                    $list_item[] = array_shift($tmp);
                }
            }
            $list[$vertex] = $list_item;
        }

        return $list;
    }

    public function getStructure() {
        $nodes = [];
        $edges = [];

        foreach ($this->vertices as $vertex) {
            $nodes[] = [
                'id' => (string) $vertex,
                'label' => (string) $vertex
            ];
        }

        foreach ($this->data as $key=>$edge) {
            $edges[] = [
                'from' => $edge[0],
                'to' => $edge[1],
                'length' => $edge[2],
                'label' => $edge[2],
                'color' => [
                    'color' => WeightedGraphController::$DEFAULT_COLOR
                ]
            ];
        }

        return [
            'nodes' => $nodes,
            'edges' => $edges
        ];
    }

    // TODO: this algorithm may be some dirty or wrong
    public function spanningTree_Borovka() {
        $min = min($this->vertices);
        $max = max($this->vertices);
        $verticesCount = count($this->vertices);
        $distanceList = $this->getDistanceList();
        $tree_id = [];
        foreach ($this->vertices as $vertex) {
            // vertex => component
            $tree_id[$vertex] = $vertex;
        }

        $res = [];
        while (count($res) < $verticesCount - 1) {
            $adjacency_list = WeightedGraphController::getAdjacencyListFromData($this->vertices, $res);
            $components = $this->getConnectivityComponentsFromData($this->vertices, $adjacency_list);

            foreach ($components as $component) {
                $edges = [];
                foreach ($component as $vertex1) {
                    foreach ($distanceList[$vertex1] as $vertex2=>$list_dist) {
                        $edge = [ $vertex1, $vertex2 ];
                        $edges[$list_dist] = $edge;
                    }
                }

                ksort($edges);

                foreach ($edges as $e) {
                    if (!in_array($e, $res) && !in_array(array_reverse($e), $res) && $tree_id[$e[0]] !== $tree_id[$e[1]]) {
                        $res[] = $e;
                        $old_id = $tree_id[$e[0]];
                        $new_id = $tree_id[$e[1]];
                        for ($j = $min; $j <= $max; ++$j) {
                            if ($tree_id[$j] === $old_id) {
                                $tree_id[$j] = $new_id;
                            }
                        }
                        break;
                    }
                }
            }
        }

        return $res;
    }

    public function getDistanceList() {
        $res = [];

        foreach ($this->data as $datum) {
            $res[$datum[0]][$datum[1]] = $datum[2];
            $res[$datum[1]][$datum[0]] = $datum[2];
        }

        return $res;
    }

    public function getDistanceMatrix() {
        $min = min($this->vertices);
        $max = max($this->vertices);

        $matrix = [];
        for ($i = $min; $i <= $max; $i++) {
            $matrix[$i] = array_fill(1, $max, PHP_INT_MAX);
        }

        foreach ($this->data as $edge) {
            $matrix[$edge[0]][$edge[1]] = $edge[2];
            $matrix[$edge[1]][$edge[0]] = $edge[2];
        }

        return $matrix;
    }

    public function getConnectivityComponentsFromData($vertices, $adjacency_list) {
        $min = min($vertices);
        $max = max($vertices);

        $res = [];

        for ($i = $min; $i <= $max; ++$i) {
            if (!$this->used_temp[$i]) {
                $this->clearTemp();
                $this->getConnectivityComponents_DFS($i, $adjacency_list);

                $res[] = $this->temp;
            }
        }

        return $res;
    }

    public function getConnectivityComponents_DFS($v, $adjacency_list) {
        $this->used_temp[$v] = true;
        $this->temp[] = $v;

        for ($i = 0; $i < count($adjacency_list[$v]); ++$i) {
            $to = $adjacency_list[$v][$i];

            if (!$this->used_temp[$to]) {
                $this->getConnectivityComponents_DFS($to, $adjacency_list);
            }
        }
    }

    public function spanningTree_Prim() {
        $min = min($this->vertices);
        $max = max($this->vertices);
        $g = $this->getDistanceMatrix();

        $used = array_fill($min, $max, false);
        $min_e = array_fill($min, $max, PHP_INT_MAX);
        $sel_e = array_fill($min, $max, -1);
        $min_e[$min] = 0;

        $res = [];

        for ($i = $min; $i <= $max; ++$i) {
            $v = -1;
            for ($j = $min; $j <= $max; ++$j) {
                if (!$used[$j] && ($v === -1 || $min_e[$j] < $min_e[$v])) {
                    $v = $j;
                }
            }
            if ($min_e[$v] === PHP_INT_MAX) {
                return false;
            }
            $used[$v] = true;
            if ($sel_e[$v] !== -1) {
                $res[] = [ $sel_e[$v], $v ];
            }
            for ($to = $min; $to <= $max; ++$to) {
                if ($g[$v][$to] < $min_e[$to]) {
                    $min_e[$to] = $g[$v][$to];
                    $sel_e[$to] = $v;
                }
            }
        }

        return $res;
    }

    public function spanningTree_Kruskal() {
        $min = min($this->vertices);
        $max = max($this->vertices);

        $cost = 0;
        $g = [];
        foreach ($this->data as $edge) {
            $g[] = [ $edge[2], [ $edge[0], $edge[1] ] ];
        }
        usort($g, function ($item1, $item2) { return $item1[0] > $item2[0]; });
        $tree_id = [];
        foreach ($this->vertices as $vertex) {
            // vertex => component
            $tree_id[$vertex] = $vertex;
        }

        $res = [];

        for ($i = 0; $i < count($g); ++$i) {
            $a = $g[$i][1][0];
            $b = $g[$i][1][1];
            $l = $g[$i][0];
            if ($tree_id[$a] !== $tree_id[$b]) {
                $cost += $l;
                $res[] = [ $a, $b ];
                $old_id = $tree_id[$b];
                $new_id = $tree_id[$a];
                for ($j = $min; $j <= $max; ++$j) {
                    if ($tree_id[$j] === $old_id) {
                        $tree_id[$j] = $new_id;
                    }
                }
            }
        }

        return $res;
    }

}
