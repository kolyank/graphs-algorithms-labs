<?php namespace Kolyank\General\Controllers;

use Exception;

class BaseController {

    public static $SPACE_DELIMITER = ' ';
    public static $NEW_LINE_DELIMITER = "\n";
    public static $DOT_DELIMITER = '.';
    public static $HYPHEN_DELIMITER = '-';
    public static $DEFAULT_COLOR = '#00BFFF';
    public static $COLORS = [
        '#7CFC00',
        '#00FA9A',
        '#008000',
        '#7B68EE',
        '#000080',
        '#8B0000',
        '#C0C0C0',
        '#FF8C00',
        '#DAA520',
        '#C71585'
    ];

    public static function readFile($file) {
        $path = implode(DIRECTORY_SEPARATOR, [
            base_path(),
            'files',
            $file
        ]);

        if (!file_exists($path)) {
            throw new Exception(sprintf('File [%s] not found ', $path));
        }

        return file_get_contents($path);
    }

    public static function getFileData($file) {
        return array_slice(array_map(function ($item) {
            return explode(self::$SPACE_DELIMITER, $item);
        }, explode(self::$NEW_LINE_DELIMITER, trim(self::readFile($file)))), 1);
    }

    public static function getDataFromAdjacencyList($list) {
        $data = [];
        foreach ($list as $vertex=>$adjacency_list) {
            foreach ($adjacency_list as $key=>$vertex2) {
                $edge = [ (string) $vertex, (string) $vertex2 ];
                if (!in_array($edge, $data) && !in_array(array_reverse($edge), $data)) {
                    $data[] = $edge;
                }
            }
        }

        return $data;
    }

    public static function getAdjacencyListFromData($vertices, $data) {
        $list = [];
        foreach ($vertices as $vertex) {
            $list_item = [];
            foreach ($data as $edge) {
                if (in_array($vertex, $edge)) {
                    $tmp = $edge;
                    unset($tmp[array_search($vertex, $edge)]);
                    $list_item[] = array_shift($tmp);
                }
            }
            $list[$vertex] = $list_item;
        }

        return $list;
    }

    public static function getStructureFromData($data) {
        $used_vertices = [];

        $edges = [];
        $nodes = [];
        foreach ($data as $edge) {
            $edges[] = [
                'from' => $edge[0],
                'to' => $edge[1],
                'color' => [
                    'color' => BaseController::$DEFAULT_COLOR
                ]
            ];

            foreach ($edge as $vertex) {
                if (!in_array($vertex, $used_vertices)) {
                    $nodes[] = [
                        'id' => (string) $vertex,
                        'label' => (string) $vertex
                    ];
                    $used_vertices[] = $vertex;
                }
            }
        }

        return [
            'nodes' => $nodes,
            'edges' => $edges
        ];
    }

    public static function array_diff_multidimensional($array1, $array2) {
        $diff = array_diff(array_map('json_encode', $array1), array_map('json_encode', $array2));
        $diff = array_map('json_decode', $diff);

        sort($diff);

        return $diff;
    }

}
