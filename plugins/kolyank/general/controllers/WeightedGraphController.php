<?php namespace Kolyank\General\Controllers;

class WeightedGraphController extends BaseController {

    public static function getVertexCountFromFile($file) {
        return (int) explode(self::$NEW_LINE_DELIMITER, self::readFile($file))[0];
    }

    public static function getDataFromAdjacencyList($list) {
        // TODO:
    }

    // [ vertex1, vertex2, weight ]
    public static function getEdgeWithoutWeight($edge) {
        return array_slice($edge, 0, -1);
    }

    public static function getMinEdge($edges) {
        usort($edges, function($item1, $item2) { return $item1[2] - $item2[2]; });

        dd($edges);

        return $edges[0];
    }

    public static function getMaxEdge($edges) {
        usort($edges, function($item1, $item2) { return $item2[2] - $item1[2]; });

        dd($edges);

        return $edges[0];
    }

}